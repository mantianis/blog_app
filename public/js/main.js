function insertMetachars(sStartTag, sEndTag) {
  var bDouble = arguments.length > 1, oMsgInput = document.getElementById("myTxtArea");
  var nSelStart = oMsgInput.selectionStart, nSelEnd = oMsgInput.selectionEnd, sOldText = oMsgInput.value;
  oMsgInput.value = sOldText.substring(0, nSelStart) + (bDouble ? sStartTag + sOldText.substring(nSelStart, nSelEnd) + sEndTag : sStartTag) + sOldText.substring(nSelEnd);
  oMsgInput.setSelectionRange(bDouble || nSelStart === nSelEnd ? nSelStart + sStartTag.length : nSelStart, (bDouble ? nSelEnd : nSelStart) + sStartTag.length);
  oMsgInput.focus();
}
//
$('.ajax').submit(function(event){
  event.preventDefault();
  var url = $(this).attr('action');
  var comment = $(this).serialize();
  $('textarea').val('')
  $.post(url, comment, function(comment){

    var date = comment.created.substring(0,10);
    event.stopPropagation();
    $('.row').append('<div class="caption-full"><div class="col-sm-3 col-md-3"><h5>' + date + '</h5><h6>Created by ' + comment.author.username + '</h6></div><div class="col-sm-9 col-sm-offset-0 col-md-9 col-md-offset-0"><h4>' + comment.text  + '</h4><div class="text-right"><a class="btn btn-xs btn-warning" href="'+url+'/comments/'+comment._id+'/edit">Edit </a> <form class="form" action="'+url+'/comments/'+comment._id+'?_method=DELETE" method="POST"><button class="btn btn-xs btn-danger">Delete</button><hr></form></div></div></div>');
  });
});
