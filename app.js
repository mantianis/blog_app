var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    methodOverride = require('method-override'),
    passport = require("passport"),
    LocalStrategy = require('passport-local'),
    User = require("./models/user"),
    flash = require('connect-flash'),
    expressSanitizer = require("express-sanitizer");

var postRoutes = require("./routes/posts"),
    commentRoutes = require("./routes/comments"),
    userRoutes = require("./routes/user"),
    messagesRoutes = require("./routes/messages");

var url = process.env.DATABASEURL || "mongodb://localhost/myBlog";
mongoose.connect(url);

app.use(flash());
app.use(methodOverride('_method'));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressSanitizer());

app.use(require('express-session')({
  secret: "This is first website that i am trying to build!",
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(User.authenticate())); //funcion from passport-local-mongoose
passport.serializeUser(User.serializeUser()); // funcion from passport-local-mongoose
passport.deserializeUser(User.deserializeUser()); // funcion from passport-local-mongoose

app.use(function(req, res, next){  // call this function on every route
  res.locals.currentUser = req.user;
  res.locals.error = req.flash("error");
  res.locals.success = req.flash("success");
  next();
});

app.use(messagesRoutes);
app.use(userRoutes);
app.use(postRoutes);
app.use(commentRoutes);


// app.listen(3030, function(){
app.listen(process.env.PORT, process.env.IP, function(){
  console.log("Server is running");
});
