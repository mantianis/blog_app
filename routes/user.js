var express = require('express'),
    router = express.Router(),
    User = require('../models/user'),
    passport = require("passport");

var admin = process.env.ADMIN;

// NEW USER
router.get("/register", function(req,res){
  res.render("auth/register")
});

router.post("/register", function(req, res){
  var newUser = new User({
    username: req.body.username,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
  });

  if(req.body.isAdmin === admin){
    newUser.isAdmin = true;
  }
  User.register(newUser, req.body.password, function(err, user){
      if(err){
        return res.render("register", {"error": err.message});
      } else {
        passport.authenticate("local")(req, res, function(){
          req.flash("success", "Welcome to my page " + user.username);
          res.redirect("/");
      });
    }
  });
});
router.get("/login", function(req,res){
  res.render("auth/login");
});
router.post("/login", passport.authenticate("local", {  //middleware
  successRedirect: "/",
  failureRedirect: "/login"
  }), function(req,res){


});
router.get("/logout", function(req, res){
  req.logout();
  req.flash("success", "Successfully logged out! ");
  res.redirect("/");
});
router.get("/about-me", function(req, res){
  res.render("auth/aboutMe");
});
module.exports = router;
