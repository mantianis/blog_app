var express = require('express'),
    router = express.Router(),
    Post = require("../models/post"),
    Comment = require("../models/comment"),
    middleWare = require("../middleware/index.js");


//new comment
router.get("/:id/comments/new", middleWare.isLoggedIn, function(req,res){
  Post.findById(req.params.id, function(err, foundPost){
    if(err){
      console.log(err);
    } else {
      res.render("comments/newComment", {post: foundPost});
    }
  });
});
router.post("/:id", middleWare.isLoggedIn, function(req, res){
  if(!req.isAuthenticated()){
    res.redirect('/login');
  }
  req.body.comment.text = req.sanitize(req.body.comment.text);
  Post.findById(req.params.id, function(err, foundPost){
    if(err){
      console.log(err);
    } else {
      Comment.create(req.body.comment, function(err, comment){
        if(err){
          console.log(err);
        } else {
            comment.author.id = req.user._id;
            comment.author.username = req.user.username;
            comment.save();
            foundPost.comments.push(comment);
            foundPost.save();
            if(req.xhr){
              res.json(comment);
            } else {
              res.redirect('/' + foundPost.id);
            }

          }
      });
    }
  });
});
//edit existing comment
router.get("/:id/comments/:comment_id/edit", middleWare.checkCommentOwnership, function(req,res){
  Comment.findById(req.params.comment_id, function(err, foundComment){
    if(err){
      console.log(err);
    } else {
      res.render("comments/editComment", {post_id: req.params.id, comment: foundComment});
    }
  });
});
router.put("/:id/comments/:comment_id/", middleWare.checkCommentOwnership, function(req,res){
  req.body.comment.text = req.sanitize(req.body.comment.text);
    Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, function(err, foundComment){
      if(err){
        console.log(err);
      } else {
        req.flash("success", "Comment was successfully edited!");
        res.redirect("/" + req.params.id);
      }
    });

});
//DELETE EXISTING Comment
router.delete("/:id/comments/:comment_id/", middleWare.checkCommentOwnership, function(req, res){
  Comment.findByIdAndRemove(req.params.comment_id, function(err){
    if(err){
      console.log(err);
    } else {
      req.flash("success", "Comment was successfully deleted!");
      res.redirect("/" + req.params.id);
    }
  });
});
module.exports = router;
