var express = require('express'),
    router = express.Router();
    Message = require('../models/contacts'),
    middleWare = require("../middleware/index.js");

router.get("/contact-me", function(req, res){
  res.render("messages/contactMe");
});
router.post("/messages", function(req, res){
  var newMessage = {
    name: req.body.name,
    email: req.body.email,
    message: req.body.message
  };
  Message.create(newMessage, function(err, newMessage){
    if(err){
      console.log(err);
    } else {
      req.flash("success", "Message was submitted");
      res.redirect("/");
    }
  });
});
router.get("/messages", middleWare.isLoggedIn, function(req, res){
  if(req.user.isAdmin){
  Message.find({}, function(err, foundMessages){
      if(err){
        console.log(err);
      } else {
        res.render("messages/messages", {message: foundMessages});
      }
  });
  } else {
  res.redirect('back');
}
});
router.delete("/messages/:id", function(req,res){
  Message.findByIdAndRemove(req.params.id, function(err){
    if(err){
      console.log(err);
    } else {
      req.flash("success", "Message was successfully deleted!");
      res.redirect("/messages");
    }
  })
});
module.exports = router;
