var express = require('express'),
    router = express.Router(),
    Post = require("../models/post"),
    middleWare = require("../middleware/index.js");

var noMatch;

router.get("/", function(req, res){
  if (req.query.search) {
       const regex = new RegExp(escapeRegex(req.query.search), 'gi');
       Post.find({title: regex}, function(err, posts){
         if(err){
           console.log(err);
         } else {
            var noMatch;
           if(posts.length < 1){
             noMatch = "No post match the query, please try again."
           }
           res.render("index", {posts: posts, noMatch: noMatch});
         }
       });
     } else {
       Post.find({}, function(err, posts){
         if(err){
           console.log(err);
         } else {
           res.render("index", {posts: posts,noMatch: noMatch});
         }
       });
     }
});

// ========= CREATE NEW POST ============
router.get("/new", middleWare.isLoggedIn, function(req, res){
  if(req.user.isAdmin){
    res.render("posts/newPost");
  } else {
    res.redirect('back');
  }
});

router.post("/new", middleWare.isLoggedIn, function(req, res){
  var newPost = {
    title: req.body.title,
    image: req.body.image,
    info: req.body.info,
    author: {
      id: req.user._id,
      username: req.user.username
    }
  }
  Post.create(newPost, function(err, newPost){
      if(err){
        req.flash("error", "Something went wrong!");
        res.redirect('back');
        console.log(err);
      } else {
        req.flash("success", "Post was added!");
        res.redirect("/");
      }
  });
});

// SHOW ONE POST
router.get("/:id", function(req,res){
  Post.findById(req.params.id).populate("comments").exec(function(err, foundPost){
    if(err){
      res.redirect('back');
      console.log(err);
    } else {
      res.render("posts/showPost", {post: foundPost});
    }
  });
});

// EDIT POST
router.get("/:id/edit", middleWare.checkPostOwnership, function(req, res){
  Post.findById(req.params.id, function(err, foundPost){
    if(err){
      req.flash("error", "Something went wrong!");
      console.log(err);
    } else {

      res.render("posts/editPost", {post: foundPost});
    }
  });
});

router.put("/:id", middleWare.checkPostOwnership, function(req,res){
  req.body.info = req.sanitize(req.body.info);
    Post.findByIdAndUpdate(req.params.id, req.body.post, function(err, updatedPost){
      if(err){
        req.flash("error", "Something went wrong!");
        res.render('back');
        console.log(err);
      } else {
        req.flash("success", "Post was edited successfully!");
        res.redirect("/" + req.params.id);
      }
    });
});

router.delete("/:id", middleWare.checkPostOwnership, function(req, res){
  Post.findByIdAndRemove(req.params.id, function(err){
    if(err){
      console.log(err);
    } else {
      console.log(req.params.id);
      req.flash("success", "Post was successfully deleted!");
      res.redirect("/");
    }
  });
});

function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};

module.exports = router;
