var middlewareObj = {};
var Post = require("../models/post");
var Comment = require("../models/comment");

middlewareObj.checkPostOwnership = function(req, res, next){
  if(req.isAuthenticated()){
    Post.findById(req.params.id, function(err, foundPost){
      if(err){
        console.log(err);
      } else {
        if(foundPost.author.id.equals(req.user._id) || req.user.isAdmin == true){
        next();
      } else {
        res.redirect('back');
        }
      }
    });
  } else {
    res.redirect('back');
  }
}
middlewareObj.checkCommentOwnership = function(req, res, next){
  if(req.isAuthenticated()){
    Comment.findById(req.params.comment_id, function(err, foundComment){
      if(err){
        console.log(err);
      } else {
        if(foundComment.author.id.equals(req.user._id) || req.user.isAdmin == true){
        next();
      } else {
        res.redirect('back');
        }
      }
    });
  } else {
    res.redirect('back');
  }
}
middlewareObj.isLoggedIn = function(req, res, next){

    if(req.isAuthenticated()){
      return next();
    }
    res.redirect("/login");
  }
module.exports = middlewareObj;
